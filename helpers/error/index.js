class MissingParamsError extends Error {
  constructor(param) {
    super()
    this.name = this.constructor.name
    this.status = 400
    this.param = param
    this.message = `Missing params ${this.param}`
  }
}

class NotFoundError extends Error {
  constructor(message) {
    super(message)
    this.name = this.constructor.name
    this.status = 404
    this.message = "Page not found"
  }
}

module.exports = { MissingParamsError, NotFoundError }
