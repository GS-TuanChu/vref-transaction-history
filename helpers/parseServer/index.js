const Parse = require("parse/node")
const APP_ID = "2blxbIsJbeR0GDgeV0pZgzGZD12MNMgLlsKqOjFW"
const JS_KEY = "HQVCDWUiUGq8Fw5I4oK1YpF9rvzVchjVi0pRHwRm"
const serverURL = "https://parseapi.back4app.com/"

class ParseServer {
  constructor() {
    Parse.initialize(APP_ID, JS_KEY)
    Parse.serverURL = serverURL
  }

  async saveBuyEvents(e) {
    const buyEvents = new Parse.Object("BuyEvents")
    const data = this.mapObject(e)
    const fields = Object.keys(data)

    fields.forEach((f) => {
      buyEvents.set(f, data[f])
    })
    try {
      await buyEvents.save()
    } catch (error) {
      console.log(error)
    }
  }

  async saveSellEvents(e) {
    const sellEvents = new Parse.Object("SellEvents")
    const data = this.mapObject(e)
    const fields = Object.keys(data)

    fields.forEach((f) => {
      sellEvents.set(f, data[f])
    })

    try {
      await sellEvents.save()
    } catch (error) {
      console.log(error)
    }
  }

  async getBuyEvents(address) {
    const buyEventsQuery = new Parse.Query("BuyEvents")
    buyEventsQuery.equalTo("address", address)
    const buyEvents = await buyEventsQuery.find()
    return buyEvents
  }

  async getSellEvents(address) {
    const sellEventsQuery = new Parse.Query("SellEvents")
    sellEventsQuery.equalTo("address", address)
    const sellEvents = await sellEventsQuery.find()
    return sellEvents
  }

  mapObject(object) {
    const {
      blockHash,
      blockNumber,
      transactionHash,
      transactionIndex,
      signature,
      returnValues: { _address, _amount },
      raw: { data, topics },
      networkName,
    } = object

    const newObject = {
      blockHash,
      blockNumber,
      transactionHash,
      transactionIndex,
      signature,
      contractAddress: object.address,
      address: _address,
      amount: _amount,
      data: data,
      topics: topics,
      networkName,
    }
    return newObject
  }
}

let _parseServer = null

function getParseServer() {
  if (!_parseServer) {
    _parseServer = new ParseServer()
  }
  return _parseServer
}
module.exports = { getParseServer }
