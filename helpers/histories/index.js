const Contract = require("../../helpers/contract")
const vref = require("../../contracts/abi.json")
const infuraId = "1a466cc6a1314f818035d806ffbf7f71"

class VrefContract extends Contract {
  constructor(network, contractAddress) {
    const infuraNetWork = `wss://${network}.infura.io/ws/v3/${infuraId}`
    super(infuraNetWork, contractAddress, vref.abi)
    this.listenTransaction()
  }

  listenTransaction() {
    this.listenBuyEvents()
    this.listenSellEvents()
  }
}

module.exports = VrefContract
