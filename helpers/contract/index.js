const Web3 = require("web3")
const { getParseServer } = require("../parseServer")

const parseServer = getParseServer()

class Contract {
  constructor(networkdId, contractAddress, abi) {
    this.contractAddress = contractAddress
    this.web3socket = new Web3(new Web3.providers.WebsocketProvider(networkdId))
    this.socketInstance = new this.web3socket.eth.Contract(abi, contractAddress)
  }

  async listenBuyEvents() {
    let networkName = null
    await this.web3socket.eth.net
      .getNetworkType()
      .then((res) => (networkName = res))
    this.socketInstance.events.buy((err, events) => {
      console.log(events)
      if (!err) {
        events.networkName = networkName
        parseServer.saveBuyEvents(events)
      }
    })
  }

  async listenSellEvents() {
    let networkName = null
    await this.web3socket.eth.net
      .getNetworkType()
      .then((res) => (networkName = res))
    this.socketInstance.events.sell((err, events) => {
      if (!err) {
        events.networkName = networkName
        parseServer.saveSellEvents(events)
      }
    })
  }

  getBuyEvents(address) {
    return parseServer.getBuyEvents(address).then((data) => data)
  }

  getSellEvents(address) {
    return parseServer.getSellEvents(address).then((data) => data)
  }
}

module.exports = Contract
