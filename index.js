const fastify = require("fastify")({ logger: true })
const address = require("./contracts/addresses.json")
const MoralisServer = require("./helpers/moralisServer/ropsten")
const { MissingParamsError, NotFoundError } = require("./helpers/error")

const networks = ["ropsten", "rinkeby", "bsc"]
const server = {}
networks.forEach((n) => {
  server[`${n}`] = new MoralisServer(n, address[n])
})

fastify.get("/buy-events/:networkName", async (request, response) => {
  const { limit, offset, fromDate, toDate } = request.query
  const { networkName } = request.params
  if (!networkName) {
    throw new MissingParamsError("networkName")
  }
  try {
    const events = await server[`${networkName}`].getBuyEvents(
      limit,
      offset,
      fromDate,
      toDate
    )
    return events
  } catch (error) {
    console.log(error)
  }
})

fastify.get("/sell-events/:networkName", async (request, response) => {
  const { limit, offset, fromDate, toDate } = request.query
  const { networkName } = request.params
  if (!networkName) {
    throw new MissingParamsError("networkName")
  }
  try {
    const events = await server[`${networkName}`].getSellEvents(
      limit,
      offset,
      fromDate,
      toDate
    )
    return events
  } catch (error) {
    console.log(error.errorMessage)
  }
})

fastify.get("*", (request, response) => {
  throw new NotFoundError()
})

async function start() {
  try {
    await fastify.listen(5000)
  } catch (error) {
    console.log(error)
  }
}

start()
